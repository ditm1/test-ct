<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>CT Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        {{--FORMS TAKEN FROM BOOTSTRAP.COM--}}
        <style>
            .header {
                margin: 40px;
            }
            .form {
                margin: 40px;
            }
            .existing {
                margin: 40px;
            }

        </style>
    </head>
    <body>
    <div class="header">
        <h1>CT Test</h1>
    </div>
        <div class="form">
            <form method="POST" action="/pr-store">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name">Product Name</label>
                    <input required type="text" class="form-control" id="name" name="name" placeholder="Enter product name">
                </div>
                <div class="form-group">
                    <label for="quantity">Product Quantity</label>
                    <input required type="number" class="form-control" id="quantity" name="quantity" placeholder="Enter quantity in stock">
                </div>
                <div class="form-group">
                    <label for="price">Product Price</label>
                    <input required type="number" class="form-control" id="price" name="price" placeholder="Enter product price">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        @if($products && $products != null)
            <div class="existing">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Price/Unit</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Date</th>
                        <th scope="col">Total</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <th scope="row">{{$product->id}}</th>
                            <td>{{$product->name}}</td>
                            <td>${{$product->price}}</td>
                            <td>{{$product->quantity}}</td>
                            <td>{{$product->created_at}}</td>
                            <td>${{$product->quantity * $product->price}}</td>
                            <td>
{{--                                <button type="button" class="btn btn-success">Edit</button>--}}
                                <form method="POST" action="/pr-delete">
                                    {{ csrf_field() }}
                                    <input type="hidden" value="{{$product->id}}" name="id">
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </body>
</html>
